package com.example.cylooq.niceview.view.fragment.search

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View
import android.view.ViewAnimationUtils
import android.view.animation.Animation
import android.view.animation.DecelerateInterpolator

/**
 * Created by matrix on 17-10-17.
 */

//搜索页面出现的动画

class SearchFragmentAnim {
    companion object {
        val DURATION: Long = 200 //持续时间
    }

    interface AnimListener {
        fun onHideAnimEnd()
        fun onShowAnimEnd()
    }

    private var animListener: AnimListener? = null

    private fun actionOtherVisible(isShow: Boolean, targetView: View, animView: View) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
            if (isShow) {
                animView.visibility = View.VISIBLE
                if (animListener != null) animListener!!.onShowAnimEnd()
            } else {
                animView.visibility = View.GONE
                if (animListener != null) animListener!!.onHideAnimEnd()

            }
            return
        }
        ///计算targetView的中心位置
        val tvLocation = IntArray(2)
        targetView.getLocationInWindow(tvLocation)
        val tvX = tvLocation[0] + targetView.width / 2
        val tvY = tvLocation[1] + targetView.height / 2

        //计算动画窗口的中心位置
        val avLocation = IntArray(2)
        animView.getLocationInWindow(avLocation)
        val avX = avLocation[0] + animView.width / 2
        val avY = avLocation[1] + animView.height / 2

        val rippleW = if (tvX < avY) animView.width - tvX else tvX - avLocation[0]
        val rippleH = if (tvY < avY) animView.height - tvY else tvY - avLocation[1]

        val maxRadius = Math.sqrt((rippleH * rippleH + rippleW * rippleW).toDouble()).toFloat()
        val starRadius: Float
        val endRadius: Float

        if (isShow) {
            starRadius = 0f
            endRadius = maxRadius
        } else {
            starRadius = maxRadius
            endRadius = 0f
        }
        val anim = ViewAnimationUtils.createCircularReveal(animView, tvX, tvY, starRadius, endRadius)
        animView.visibility = View.VISIBLE
        anim.duration = DURATION //动画时长
        anim.interpolator = DecelerateInterpolator() //定义动画变化率 dec变慢了

        anim.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                super.onAnimationEnd(animation)
                if (isShow) {
                    animView.visibility = View.VISIBLE
                    if (animListener != null) animListener!!.onShowAnimEnd()
                } else {
                    animView.visibility = View.GONE
                    if (animListener != null) animListener!!.onHideAnimEnd()

                }
            }
        })
        anim.start()
    }

    fun show(targetView: View, showView: View) {
        actionOtherVisible(true, targetView, showView)
    }

    fun hide(targetView: View, hideView: View) {
        actionOtherVisible(false, targetView, hideView)
    }

    fun setListener(listener: AnimListener) {
        animListener = listener
    }

}