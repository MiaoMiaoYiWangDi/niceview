package com.example.cylooq.niceview.view

import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.example.cylooq.niceview.R
import com.example.cylooq.niceview.adatper.DownloadAdatper
import com.example.cylooq.niceview.model.bean.VideoBean
import com.example.cylooq.niceview.utils.ObjectSaveUtils
import com.example.cylooq.niceview.utils.SharedPreferenceUtils
import com.gyf.barlibrary.ImmersionBar
import kotlinx.android.synthetic.main.active_down_history.*
import zlc.season.rxdownload2.RxDownload


/**
 * Created by matrix on 17-10-18.
 */
class DownloadActivity : AppCompatActivity() {
    var lists: ArrayList<VideoBean> = ArrayList<VideoBean>()
    lateinit var downlaodAdatper: DownloadAdatper
    var handler: Handler = object : Handler() {
        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)
            var list = msg?.data?.getParcelableArrayList<VideoBean>("beans")
            if (list?.size?.compareTo(0) == 0) {
                //没有缓存视频
                down_history_hint.visibility = View.VISIBLE
            } else {
                down_history_hint.visibility = View.GONE
                if (lists?.size > 0) {
                    lists.clear()
                }
                list?.let { lists.addAll(it) }
                downlaodAdatper.notifyDataSetChanged()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.active_down_history)
        ImmersionBar.with(this).transparentBar().barAlpha(0f).navigationBarAlpha(0.5f)
                .statusBarDarkFont(true).fitsSystemWindows(true).init()
        setToolBar()
        DataAsyncTask(handler, this).execute()
        down_history_recyclerView.layoutManager = LinearLayoutManager(this)
        downlaodAdatper = DownloadAdatper(this, lists)
        downlaodAdatper.setOnLongClickListener(object : DownloadAdatper.OnLongClickListener {
            override fun onLongClick(position: Int) {
                addDialog(position)
            }
        })
        down_history_recyclerView.adapter = downlaodAdatper


    }

    private fun setToolBar() {
        setSupportActionBar(down_history_toolbar)
        var bar = supportActionBar
        bar?.title = "我的缓存"
        bar?.setDisplayHomeAsUpEnabled(true)

        down_history_toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun addDialog(position: Int) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("是否删除当前视频")
        builder.setNegativeButton("否", {
            dialog, which ->
            dialog.dismiss()
        })
        builder.setPositiveButton("是", {
            dialog, which ->
            deletctDownload(position)
        })
        builder.show()
    }

    private fun deletctDownload(position: Int) {

        RxDownload.getInstance(this@DownloadActivity)
                .deleteServiceDownload(lists[position].playUrl, true).subscribe()
        SharedPreferenceUtils.getInstance(this, "downloads")
                .put(lists[position].playUrl.toString(), "")
        val count = position + 1
        ObjectSaveUtils.delectFile("download$count", this)
        lists.removeAt(position)
        downlaodAdatper.notifyDataSetChanged()
    }

    //从存储目录读取数据
    private class DataAsyncTask(handler: Handler, activity: DownloadActivity) : AsyncTask<Void, Void, ArrayList<VideoBean>>() {
        val activity: DownloadActivity = activity
        val handler: Handler = handler
        override fun doInBackground(vararg params: Void?): ArrayList<VideoBean>? {
            var list: ArrayList<VideoBean> = ArrayList<VideoBean>()
            var count: Int = SharedPreferenceUtils.getInstance(activity, "downloads").getInt("count")
            var i = 1
            while (i.compareTo(count) <= 0) {
                var bean: VideoBean
                if (ObjectSaveUtils.getValue(activity, "download$i") == null) {
                    i++
                    continue
                } else {
                    bean = ObjectSaveUtils.getValue(activity, "download$i") as VideoBean
                }
                list.add(bean)
                i++
            }
            return list
        }

        override fun onPostExecute(result: ArrayList<VideoBean>?) {
            super.onPostExecute(result)
            val message = handler.obtainMessage()
            val bundle = Bundle()
            bundle.putParcelableArrayList("beans", result)
            message.data = bundle
            handler.sendMessage(message)
        }
    }
}