package com.example.cylooq.niceview.adatper

import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.cylooq.niceview.R
import com.example.cylooq.niceview.model.bean.RankBean
import com.example.cylooq.niceview.model.bean.VideoBean
import com.example.cylooq.niceview.utils.ImageLoadUtils
import com.example.cylooq.niceview.utils.ObjectSaveUtils
import com.example.cylooq.niceview.utils.SharedPreferenceUtils
import com.example.cylooq.niceview.view.VideoDetailActivity
import kotlinx.android.synthetic.main.item_rank.view.*

/**
 * Created by RuiChang on 2017/08/28.
 * 热点
 */
class RankAdatper(context: Context,list:ArrayList<RankBean.ItemListBean.DataBean>):RecyclerView.Adapter<RankAdatper.RankViewHolder>(){
    var contexts:Context?=null
    var dataList:ArrayList<RankBean.ItemListBean.DataBean>?=null
    var inflater:LayoutInflater?=null
    init {
        contexts=context
        dataList=list
        inflater= LayoutInflater.from(contexts)
    }


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RankViewHolder {
        return RankViewHolder(inflater?.inflate(R.layout.item_rank,parent,false),contexts!!)
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun onBindViewHolder(holder: RankViewHolder?, position: Int) {
        var photoUrl:String?=dataList?.get(position)?.cover?.feed
        photoUrl?.let { ImageLoadUtils.displyImage(contexts!!,holder?.video_img_rank,it) }
        var videoTitle:String?=dataList?.get(position)?.title
        holder?.video_title_rank?.text=videoTitle
        var category:String?=dataList?.get(position)?.category
        var duration=dataList?.get(position)?.duration
        var minute =duration?.div(60)
        var second = duration?.minus((minute?.times(60)) as Long )
        var realMinute : String
        var realSecond : String
        if(minute!!<10){
            realMinute = "0"+minute.toString()
        }else{
            realMinute = minute.toString()
        }
        if(second!!<10){
            realSecond = "0"+second.toString()
        }else{
            realSecond = second.toString()
        }
        holder?.video_detail_rank?.text="$category / $realMinute'$realSecond''"
        //跳转到视频详情页
        holder?.itemView?.setOnClickListener{

            var intent: Intent =Intent(contexts,VideoDetailActivity::class.java)
            var id:Int=dataList?.get(position)?.id?:0
            var videoDescription=dataList?.get(position)?.description
            var videoPlayUrl=dataList?.get(position)?.playUrl
            var videoBlurred=dataList?.get(position)?.cover?.blurred
            var videoCollectionCount=dataList?.get(position)?.consumption?.collectionCount
            var videoShareCount=dataList?.get(position)?.consumption?.shareCount
            var videoReplayCount=dataList?.get(position)?.consumption?.replyCount
            var time=System.currentTimeMillis()
            var videoBean=VideoBean(id,photoUrl,videoTitle,videoDescription,duration,videoPlayUrl,
                    category,videoBlurred,videoCollectionCount,videoShareCount,videoReplayCount,time)
            var cacheUrl= SharedPreferenceUtils.getInstance(contexts!!,"beans").getString(videoPlayUrl!!)
            if (cacheUrl.equals("")){
                var count= SharedPreferenceUtils.getInstance(contexts!!,"bean").getInt("count")
                if(count==-1){
                    count=count.inc()
                }else{
                    count=1
                }
                SharedPreferenceUtils.getInstance(contexts!!,"beans").put("count",count)
                SharedPreferenceUtils.getInstance(contexts!!,"beans").put(videoPlayUrl!!,videoPlayUrl)
                ObjectSaveUtils.saveObject(contexts!!,"bean$count",videoBean)

            }
            intent.putExtra("data",videoBean as Parcelable)
            contexts?.let { contexts ->contexts.startActivity(intent)  }

        }




    }
    class RankViewHolder(itemView:View?,context: Context):RecyclerView.ViewHolder(itemView){
        var video_img_rank:ImageView?=null
        var video_title_rank:TextView?=null
        var video_detail_rank:TextView?=null
        init {
            video_img_rank=itemView?.findViewById<ImageView>(R.id.video_img_rank)
            video_title_rank=itemView?.findViewById<TextView>(R.id.video_title_rank)
            video_detail_rank=itemView?.findViewById<TextView>(R.id.video_detail_rank)
        }
    }
}