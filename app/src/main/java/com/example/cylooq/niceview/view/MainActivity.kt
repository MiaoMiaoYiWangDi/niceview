package com.example.cylooq.niceview.view

/**
 * Created by RuiChang on 2017/08/22.
 * This Activity is Main interface
 */

import android.support.v7.app.AppCompatActivity
import android.support.v4.app.Fragment
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.KeyEvent
import kotlinx.android.synthetic.main.activity_main.*
import android.view.View
import android.widget.Toast
import com.example.cylooq.niceview.R
import com.example.cylooq.niceview.utils.ToastMine
import com.example.cylooq.niceview.view.fragment.DiscoverFragment
import com.example.cylooq.niceview.view.fragment.HomeFragment
import com.example.cylooq.niceview.view.fragment.HotFragment
import com.example.cylooq.niceview.view.fragment.MeFragment
import com.example.cylooq.niceview.view.fragment.search.SEARCH_TAG
import com.example.cylooq.niceview.view.fragment.search.SearchFragment


class MainActivity : AppCompatActivity(), View.OnClickListener {

    var homeFragment: HomeFragment? = null
    var meFragment: MeFragment? = null
    var discoverFragment: DiscoverFragment? = null
    var hotFragment: HotFragment? = null
    lateinit var searchFragment: SearchFragment
    var exitTime:Long=0
    var toast: Toast? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
/*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility( View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
            getWindow().getDecorView().setSystemUiVisibility( View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
        }
*/
        setRadioGroupButton()
        initFragment(savedInstanceState)
        initToolbar()
    }


    override fun onPause() {
        super.onPause()
        toast?.let { toast!!.cancel() }
    }

     override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
            if(keyCode== KeyEvent.KEYCODE_BACK){
                if(System.currentTimeMillis().minus(exitTime)<=2000){
                    finish()
                    toast?.cancel()
                }else{
                    exitTime= System.currentTimeMillis()
                   // toast=showToast("再次点击退出应用")
                    ToastMine(this,"再次点击退出应用",Toast.LENGTH_SHORT)
                }
                return true
            }
            return super.onKeyDown(keyCode, event)
        }
    private fun initToolbar() {
        title_bar_main.text = getString(R.string.Daily)
        searchFragment = SearchFragment()
        //title_bar_main.typeface = Typeface.createFromAsset(this.assets, "fonts/Lobster-1.4.otf")
        icon_searchOrSetting_main.setImageResource(R.drawable.icon_search)
        icon_searchOrSetting_main.setOnClickListener {
            if (icon_searchOrSetting_main.visibility == View.VISIBLE && !rb_me_main.isChecked) {

                searchFragment.show(fragmentManager, SEARCH_TAG)
            }
        }
        icon_search_main.setOnClickListener{
            if(icon_search_main.visibility == View.VISIBLE){
                searchFragment.show(fragmentManager, SEARCH_TAG)
            }
        }

    }

    private fun setRadioGroupButton() {
        rb_home_main.isChecked = true
        rb_home_main.setTextColor(ContextCompat.getColor(this, R.color.eggplant))
        rb_discover_main.setTextColor(ContextCompat.getColor(this, R.color.bf))
        rb_popular_main.setTextColor(ContextCompat.getColor(this, R.color.bf))
        rb_me_main.setTextColor(ContextCompat.getColor(this, R.color.bf))
        rb_me_main.setOnClickListener(this)
        rb_home_main.setOnClickListener(this)
        rb_discover_main.setOnClickListener(this)
        rb_popular_main.setOnClickListener(this)
    }

    protected fun reSetRadioGroupButton() {
        rg_main.clearCheck()
        rb_home_main.setTextColor(ContextCompat.getColor(this, R.color.bf))
        rb_discover_main.setTextColor(ContextCompat.getColor(this, R.color.bf))
        rb_popular_main.setTextColor(ContextCompat.getColor(this, R.color.bf))
        rb_me_main.setTextColor(ContextCompat.getColor(this, R.color.bf))
    }


    private fun initFragment(savedInstanceState: Bundle?) {
        Log.d("fragment", "初始化")
        if (savedInstanceState != null) {
            //异常情况
            val mFragments: List<Fragment> = supportFragmentManager.fragments
            for (item in mFragments) {
                if (item is HomeFragment) {
                    homeFragment = item
                }
                if (item is MeFragment) {
                    meFragment = item
                }
                if (item is DiscoverFragment) {
                    discoverFragment = item
                }
                if (item is HotFragment) {
                    hotFragment = item
                }
            }

        } else {
            homeFragment = HomeFragment()
            meFragment = MeFragment()
            discoverFragment = DiscoverFragment()
            hotFragment = HotFragment()
            val fragmentTrans = supportFragmentManager.beginTransaction()
            fragmentTrans.add(R.id.framelayout_home, meFragment)
            fragmentTrans.add(R.id.framelayout_home, homeFragment)
            fragmentTrans.add(R.id.framelayout_home, discoverFragment)
            fragmentTrans.add(R.id.framelayout_home, hotFragment)
            fragmentTrans.commit()


        }
        supportFragmentManager.beginTransaction().show(homeFragment)
                .hide(meFragment)
                .hide(discoverFragment)
                .hide(hotFragment)
                .commit()
    }

    override fun onClick(v: View?) {
        reSetRadioGroupButton()
        when (v?.id) {
            R.id.rb_home_main -> {
                rb_home_main.isChecked = true
                rb_home_main.setTextColor(ContextCompat.getColor(this, R.color.eggplant))
                supportFragmentManager.beginTransaction().show(homeFragment)
                        .hide(meFragment)
                        .hide(discoverFragment)
                        .hide(hotFragment)
                        .commit()

                icon_searchOrSetting_main.setImageResource(R.drawable.icon_search)
                icon_searchOrSetting_main.visibility = View.VISIBLE

                icon_search_main.visibility = View.INVISIBLE

                title_bar_main.text = getString(R.string.Daily)
                title_bar_main.visibility = View.VISIBLE
            }
            R.id.rb_me_main -> {
                rb_me_main.isChecked = true
                rb_me_main.setTextColor(ContextCompat.getColor(this, R.color.eggplant))
                supportFragmentManager.beginTransaction().show(meFragment)
                        .hide(homeFragment)
                        .hide(discoverFragment)
                        .hide(hotFragment)
                        .commit()
                icon_searchOrSetting_main.visibility = View.VISIBLE
                icon_searchOrSetting_main.setImageResource(R.drawable.icon_setting)
                icon_search_main.visibility = View.INVISIBLE
                icon_search_main.visibility = View.INVISIBLE
                title_bar_main.visibility = View.INVISIBLE
            }
            R.id.rb_discover_main -> {
                rb_discover_main.isChecked = true
                rb_discover_main.setTextColor(ContextCompat.getColor(this, R.color.eggplant))
                supportFragmentManager.beginTransaction().show(discoverFragment)
                        .hide(homeFragment)
                        .hide(hotFragment)
                        .hide(meFragment)
                        .commit()
                icon_searchOrSetting_main.visibility = View.INVISIBLE
                icon_search_main.visibility = View.VISIBLE
                icon_search_main.setImageResource(R.drawable.icon_search)
                title_bar_main.text = getString(R.string.Discover)
                title_bar_main.visibility = View.VISIBLE
            }
            R.id.rb_popular_main -> {
                rb_popular_main.isChecked = true
                rb_popular_main.setTextColor(ContextCompat.getColor(this, R.color.eggplant))
                supportFragmentManager.beginTransaction().show(hotFragment)
                        .hide(homeFragment)
                        .hide(discoverFragment)
                        .hide(meFragment)
                        .commit()
                icon_searchOrSetting_main.visibility = View.INVISIBLE
                icon_search_main.visibility = View.VISIBLE
                icon_search_main.setImageResource(R.drawable.icon_search)
                title_bar_main.text = getString(R.string.Popular)
                title_bar_main.visibility = View.VISIBLE
                title_bar_main.text = getString(R.string.Popular)
                title_bar_main.visibility = View.VISIBLE
            }
        }
    }

}