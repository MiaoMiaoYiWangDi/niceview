package com.example.cylooq.niceview.adatper

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Parcelable
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.cylooq.niceview.R
import com.example.cylooq.niceview.model.bean.VideoBean
import com.example.cylooq.niceview.utils.ImageLoadUtils
import com.example.cylooq.niceview.utils.SharedPreferenceUtils
import com.example.cylooq.niceview.view.VideoDetailActivity
import io.reactivex.disposables.Disposable
import zlc.season.rxdownload2.RxDownload
import zlc.season.rxdownload2.entity.DownloadFlag
import java.text.SimpleDateFormat

/**
 * Created by RuiChang on 2017/08/30.
 */
class HistoryAdatper(context: Context, list: ArrayList<VideoBean>) : RecyclerView.Adapter<HistoryAdatper.DownloadViewHolder>() {
    var context: Context? = null;
    var list: ArrayList<VideoBean>? = null
    var inflater: LayoutInflater? = null
    var hasLoaded = false
    var text:String?=null
    init {
        this.context = context
        this.list = list
        this.inflater = LayoutInflater.from(context)
        this.text=context.getString(R.string.Release)?:""
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): DownloadViewHolder {
        return DownloadViewHolder(inflater?.inflate(R.layout.item_history, parent, false), context!!)
    }

    override fun getItemCount(): Int {
        return list?.size ?: 0
    }

    override fun onBindViewHolder(holder: DownloadViewHolder?, position: Int) {
        var photoUrl: String? = list?.get(position)?.feed
        photoUrl?.let { ImageLoadUtils.displyImage(context!!, holder?.iv_photo, it) }
        var title: String? = list?.get(position)?.title
        holder?.tv_title?.text = title
        var category = list?.get(position)?.category
        var duration=list?.get(position)?.duration
        var minute = list?.get(position)?.duration?.div(60)
        var second = list?.get(position)?.duration?.minus(minute?.times(60) as Long)
        var realMinute: String
        var realSecond: String
        if (minute!! < 10) {
            realMinute = "0" + minute.toString()
        } else {
            realMinute = minute.toString()
        }
        if (second!! < 10) {
            realSecond = "0" + second.toString()
        } else {
            realSecond = second.toString()
        }
        holder?.tv_detail?.text="$text $category /$realMinute:$realSecond"

        holder?.itemView?.setOnClickListener {
            //跳转视频详情页
            var intent: Intent = Intent(context, VideoDetailActivity::class.java)
            var id=list?.get(position)?.id?:0
            var desc = list?.get(position)?.description
            var playUrl = list?.get(position)?.playUrl
            var blurred = list?.get(position)?.blurred
            var collect = list?.get(position)?.collect
            var share = list?.get(position)?.share
            var reply = list?.get(position)?.reply
            var time = System.currentTimeMillis()
            var videoBean = VideoBean(id,photoUrl, title, desc, duration, playUrl, category, blurred, collect, share, reply, time)
            intent.putExtra("data", videoBean as Parcelable)

            context?.let { context -> context.startActivity(intent) }
        }
    }




    class DownloadViewHolder(itemView: View?, context: Context) : RecyclerView.ViewHolder(itemView) {
        var iv_photo: ImageView = itemView?.findViewById<ImageView>(R.id.video_history_photo) as ImageView
        var tv_title: TextView = itemView?.findViewById<TextView>(R.id.history_title) as TextView
        var tv_detail: TextView = itemView?.findViewById<TextView>(R.id.history_detail) as TextView

    }

}