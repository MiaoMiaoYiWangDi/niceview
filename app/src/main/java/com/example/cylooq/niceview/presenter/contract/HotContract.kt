package com.example.cylooq.niceview.presenter.contract

import com.example.cylooq.niceview.model.bean.RankBean
import com.example.cylooq.niceview.presenter.base.BasePresenter
import com.example.cylooq.niceview.presenter.base.BaseView

/**
 * Created by RuiChang on 2017/08/29.
 */
interface HotContract{
    interface Presenter:BasePresenter{
        fun requestData(strategy:String)
    }
    interface View: BaseView<Presenter> {
        fun setData(bean:RankBean)
    }
}