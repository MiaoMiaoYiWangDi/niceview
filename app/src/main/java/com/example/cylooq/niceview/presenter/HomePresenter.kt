package com.example.cylooq.niceview.presenter

import android.content.Context
import com.example.cylooq.niceview.model.bean.HomeBean
import com.example.cylooq.niceview.model.bean.HomeModel
import com.example.cylooq.niceview.presenter.contract.HomeContract
import com.example.cylooq.niceview.utils.applySchedulers
import io.reactivex.Observable

/**
 * Created by RuiChang on 2017/08/25.
 * z主页
 */
class HomePresenter(context: Context, view: HomeContract.View) : HomeContract.Presenter {
    var mContext: Context? = null
    var mView: HomeContract.View? = null
    val mModel: HomeModel by lazy {
        //懒加载
        HomeModel()
    }

    init {
        mContext = context
        mView = view
    }

    override fun start() {
        requestData()
    }

    override fun requestData() {
        val observable: Observable<HomeBean>? = mContext?.let { mModel.loadData(it, true, "0") }
        observable?.applySchedulers()?.subscribe { homeBean: HomeBean ->
            mView?.setData(homeBean)
        }
    }
    fun moreData(data :String?){
        val observable:Observable<HomeBean>?=mContext?.let { mModel.loadData(it,false,data) }
        observable?.applySchedulers()?.subscribe{homeBean:HomeBean->
            mView?.setData(homeBean)
        }

    }

}