package com.example.cylooq.niceview.model

import android.content.Context
import com.example.cylooq.niceview.model.bean.DiscoverBean
import com.example.cylooq.niceview.network.ApiService
import com.example.cylooq.niceview.network.RetrofitClient
import io.reactivex.Observable

/**
 * Created by RuiChang on 2017/08/28.
 */
class DiscoverModel{
    fun loadData(context: Context):Observable<MutableList<DiscoverBean>>?{

        val retrofitClient= RetrofitClient.getInstance(context,ApiService.BASE_URL)
        val apiService=retrofitClient.create(ApiService::class.java)
        return apiService?.getFindData()
    }
}