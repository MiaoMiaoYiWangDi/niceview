package com.example.cylooq.niceview.view

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.example.cylooq.niceview.R
import com.example.cylooq.niceview.adatper.ResultAdatper
import com.example.cylooq.niceview.model.bean.RankBean
import com.example.cylooq.niceview.presenter.SearchResultPresenter
import com.example.cylooq.niceview.presenter.contract.SearchResultContract
import com.gyf.barlibrary.ImmersionBar
import kotlinx.android.synthetic.main.active_result.*

/**
 * Created by matrix on 17-10-18.
 */
class SearchResultActivity: AppCompatActivity(), SearchResultContract.View, SwipeRefreshLayout.OnRefreshListener{

    lateinit var searchWord:String
    lateinit var searchResultPresenter:SearchResultPresenter
    lateinit var searchResultAdatper:ResultAdatper
    var isRefresh:Boolean=false
    var lists=ArrayList<RankBean.ItemListBean.DataBean>()
    var start:Int=10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.active_result)

        ImmersionBar.with(this).transparentBar().barAlpha(0f).navigationBarAlpha(0.5f).statusBarDarkFont(true).fitsSystemWindows(true).init()

        searchWord=intent.getStringExtra("keyWord")

        searchResultPresenter= SearchResultPresenter(this,this)
        searchResultPresenter.requestData(searchWord,start)
        setToolBar()

        result_recyclerView.layoutManager=LinearLayoutManager(this)
        searchResultAdatper= ResultAdatper(this,lists)
        result_recyclerView.adapter=searchResultAdatper

        result_refreshLayout.setOnRefreshListener(this)
        result_recyclerView.addOnScrollListener(object :RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                var layoutManager:LinearLayoutManager=result_recyclerView.layoutManager as LinearLayoutManager
                var lastPosition=layoutManager.findLastVisibleItemPosition()
                if(newState==RecyclerView.SCROLL_STATE_IDLE && lastPosition == lists?.size -1){
                    start=start.plus(10)
                    searchResultPresenter.requestData(searchWord,start)
                }
            }
        })

    }
    override fun setData(bean: RankBean) {
        if(isRefresh){
            isRefresh=false
            result_refreshLayout.isRefreshing=false
            if (lists?.size > 0){
                lists.clear()
            }
        }
        bean?.itemList?.forEach{
            it.data?.let { it1->lists.add(it1) }
        }
        searchResultAdatper.notifyDataSetChanged()
    }

    override fun onRefresh() {
        if (isRefresh){
            isRefresh=true
            start=10
            searchResultPresenter.requestData(searchWord,start)
        }
    }
    private fun setToolBar(){
        setSupportActionBar(result_toolbar)
        var bar=supportActionBar
        bar?.title="$searchWord 搜索结果"
        bar?.setDisplayHomeAsUpEnabled(true)

        result_toolbar.setNavigationOnClickListener{
            onBackPressed()
        }
    }
}