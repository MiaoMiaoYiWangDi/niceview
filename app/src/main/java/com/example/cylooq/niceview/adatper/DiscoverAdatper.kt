package com.example.cylooq.niceview.adatper

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.cylooq.niceview.R
import com.example.cylooq.niceview.model.bean.DiscoverBean
import com.example.cylooq.niceview.utils.ImageLoadUtils

/**
 * Created by RuiChang on 2017/08/28.
 */
class DiscoverAdatper(contexts: Context, list: MutableList<DiscoverBean>?) : BaseAdapter() {
    var context: Context? = null
    var discoverList: MutableList<DiscoverBean>? = null
    var inflater: LayoutInflater? = null

    init {
        this.context = contexts
        this.discoverList = list
        this.inflater = LayoutInflater.from(context)
    }

    override fun getView(position: Int, convertView: View?, parentView: ViewGroup?): View {
        var view: View
        var discoverViewHolder: DiscoverViewHolder
        if (convertView == null) {
            view = inflater!!.inflate(R.layout.item_discover, parentView, false)
            discoverViewHolder = DiscoverViewHolder(view)
            view.tag = discoverViewHolder//viewholder存储在view中
        } else {
            view = convertView//缓存的view不为空的话
            discoverViewHolder = view.tag as DiscoverViewHolder
        }

        ImageLoadUtils.displyImage(context!!, discoverViewHolder.img_discover, discoverList?.get(position)?.bgPicture!!)
        discoverViewHolder?.title_discover?.text = discoverList?.get(position)!!.name

        return view
    }

    override fun getCount(): Int {
        if (discoverList != null) {
            return discoverList!!.size
        } else {
            return 0
        }
    }

    override fun getItem(p0: Int): DiscoverBean? {
        return discoverList?.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return p0?.toLong()
    }

    class DiscoverViewHolder(itemView: View) {
        var img_discover: ImageView? = null
        var title_discover: TextView? = null

        init {
            img_discover = itemView?.findViewById<ImageView>(R.id.img_discover)
            title_discover = itemView?.findViewById<TextView>(R.id.title_discover)
        }

    }
}