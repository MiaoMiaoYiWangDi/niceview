package com.example.cylooq.niceview.model

import android.content.Context
import com.example.cylooq.niceview.model.bean.RankBean
import com.example.cylooq.niceview.network.ApiService
import com.example.cylooq.niceview.network.RetrofitClient
import io.reactivex.Observable

/**
 * Created by matrix on 17-10-17.
 */
class SearchResultModel{
    fun loadData(context: Context, query: String, start: Int):Observable<RankBean>?{
        val retrofitClient = RetrofitClient.getInstance(context, ApiService.BASE_URL)
        val apiService = retrofitClient.create(ApiService::class.java)
        return apiService?.getSearchData(10,query,start)
    }
}