package com.example.cylooq.niceview.model.bean

/**
 * Created by RuiChang on 2017/08/29.
 */
data class ReplayBean(var count:Int,var total :Int,var nextPageUrl:String?,
                      var replyList:List<ReplayListBean>?){
    data class ReplayListBean(var id:Long,var videoId:Int,var videoTitle:String ,var parentReplyId:Long,
                              var rootReplyId:Long ,var sequence:Int,var message:String?,var replyStatus:String?,
                              var createTime:Long? ,var user:List<UserListBean>?,
                              var likeCount:Int,var liked:Boolean,var hot:Boolean,var userType:Any?,
                              var type:String?,var sid:Any?,var userBlocked:Boolean){
        data class UserListBean(var uid:Long,var nickname:String?,var avatar:String?,
                                var userType:String?,var ifPgc:Boolean)
    }
}