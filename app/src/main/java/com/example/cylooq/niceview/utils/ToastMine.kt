package com.example.cylooq.niceview.utils

import android.content.Context
import android.view.Gravity

import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.example.cylooq.niceview.R

/**
 * Created by matrix on 17-10-18.
 */
class ToastMine(context: Context,text:String,duration:Int){
    var tostMine:Toast
    var views:View
    var textViews:TextView
    init {
        views=LayoutInflater.from(context).inflate(R.layout.toast_mine,null)
        textViews=views?.findViewById(R.id.toast_text)
        textViews.setText(text)
        tostMine= Toast(context)
        tostMine.duration=duration
        tostMine.view=views
        tostMine.gravity
        tostMine.show()

    }

}