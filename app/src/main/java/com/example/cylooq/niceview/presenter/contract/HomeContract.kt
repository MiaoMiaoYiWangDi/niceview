package com.example.cylooq.niceview.presenter.contract

import com.example.cylooq.niceview.model.bean.HomeBean
import com.example.cylooq.niceview.presenter.base.BasePresenter
import com.example.cylooq.niceview.presenter.base.BaseView

/**
 * Created by RuiChang on 2017/08/25.
 */
interface HomeContract{
    interface View : BaseView<Presenter> {
        fun setData(bean : HomeBean)
    }
    interface Presenter : BasePresenter {
        fun requestData()
    }
}