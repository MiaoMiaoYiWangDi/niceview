package com.example.cylooq.niceview.model

import android.content.Context
import com.example.cylooq.niceview.model.bean.RankBean
import com.example.cylooq.niceview.network.ApiService
import com.example.cylooq.niceview.network.RetrofitClient
import io.reactivex.Observable

/**
 * Created by RuiChang on 2017/08/28.
 */
class DiscoverDetailModel{
    fun loadData(context: Context,categoryName:String,strategy:String?):Observable<RankBean>?{
        var retrofitClient= RetrofitClient.getInstance(context,ApiService.BASE_URL)
        val apiService=retrofitClient.create(ApiService::class.java)
        return apiService?.getFindDetailData(categoryName, strategy!!, "26868b32e808498db32fd51fb422d00175e179df", 83)
    }
    fun loadMoreData(context: Context,start : Int, categoryName: String, strategy: String?):Observable<RankBean>?{
        var retrofitClient=RetrofitClient.getInstance(context,ApiService.BASE_URL)
        var apiService=retrofitClient.create(ApiService::class.java)
        return apiService?.getFindDetailMoreData(start,10,categoryName, strategy!!)
    }
}