package com.example.cylooq.niceview.view.fragment

import android.support.v4.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by RuiChang on 2017/08/24.
 * 基础fragment
 */
abstract class BaseFragment : Fragment() {
    var isFirst: Boolean = false
    var fragmentView: View? = null
    var isVisiable: Boolean = false
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (fragmentView == null) {
            fragmentView = inflater?.inflate(getLayoutResources(), container, false)
        }
        return fragmentView
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)

        if(isVisibleToUser){
            isVisiable==true
        }
        if(fragmentView==null){
            return
        }
        //可见且首次加载
        if(!isFirst&&isVisiable){
            onFragementVisiableChang(true)
            return
        }
        //由可见变不可见,y已加载
        if(isVisiable){
            onFragementVisiableChang(false)
            isVisiable=false
            return
        }
    }
    open protected fun onFragementVisiableChang(b: Boolean) {

    }

    //抽象函数，获取Layout里的xml资源文件
    abstract fun getLayoutResources(): Int
    //抽象函数，视图创建后执行加载视图的操作
    abstract fun initView()
}