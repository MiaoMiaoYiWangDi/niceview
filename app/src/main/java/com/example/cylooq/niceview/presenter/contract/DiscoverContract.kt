package com.example.cylooq.niceview.presenter.contract

import com.example.cylooq.niceview.model.bean.DiscoverBean
import com.example.cylooq.niceview.presenter.base.BasePresenter
import com.example.cylooq.niceview.presenter.base.BaseView

/**
 * Created by RuiChang on 2017/08/28.
 */

interface DiscoverContract{
    interface View:BaseView<Presenter>{
        fun setData(beans:MutableList<DiscoverBean>)
    }interface Presenter:BasePresenter{
        fun requestData()
    }
}