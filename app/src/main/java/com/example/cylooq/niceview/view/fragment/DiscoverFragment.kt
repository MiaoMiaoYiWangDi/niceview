package com.example.cylooq.niceview.view.fragment

import android.content.Intent
import android.util.Log
import com.example.cylooq.niceview.R
import com.example.cylooq.niceview.adatper.DiscoverAdatper
import com.example.cylooq.niceview.model.bean.DiscoverBean
import com.example.cylooq.niceview.presenter.DiscoverPresenter
import com.example.cylooq.niceview.presenter.contract.DiscoverContract
import com.example.cylooq.niceview.view.DiscoverDetailActivity
import kotlinx.android.synthetic.main.discover_fragment.*
import okhttp3.internal.Internal

/**
 * Created by RuiChang on 2017/08/28.
 */
class DiscoverFragment:BaseFragment(),DiscoverContract.View {
    var discoverPresenter: DiscoverPresenter?=null
    var discoverAdatper: DiscoverAdatper?=null
    var discoverList:MutableList<DiscoverBean>?=null

    override fun initView() {
        discoverPresenter= DiscoverPresenter(context,this)
        discoverPresenter?.start()
        discoverAdatper= DiscoverAdatper(context,discoverList)
        gridview_discover.adapter=discoverAdatper

        gridview_discover.setOnItemClickListener{ parent, view, position, id->
            var bean=discoverList?.get(position)
            var name=bean?.name.toString()
            var intent= Intent(context,DiscoverDetailActivity::class.java)
            intent.putExtra("name",name)
            startActivity(intent)
        }
    }

    override fun getLayoutResources(): Int {
        return R.layout.discover_fragment
    }

    override fun setData(beans: MutableList<DiscoverBean>) {
        discoverAdatper?.discoverList=beans
        discoverList=beans
        discoverAdatper?.notifyDataSetChanged()
    }
}