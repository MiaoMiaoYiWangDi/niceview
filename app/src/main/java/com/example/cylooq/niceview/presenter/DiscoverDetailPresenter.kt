package com.example.cylooq.niceview.presenter

import android.content.Context

import com.example.cylooq.niceview.model.DiscoverDetailModel
import com.example.cylooq.niceview.model.DiscoverModel
import com.example.cylooq.niceview.model.bean.RankBean
import com.example.cylooq.niceview.presenter.contract.DiscoverDetailContract
import com.example.cylooq.niceview.utils.applySchedulers
import io.reactivex.Observable

/**
 * Created by RuiChang on 2017/08/28.
 */
class DiscoverDetailPresenter(context: Context,view:DiscoverDetailContract.View):DiscoverDetailContract.Presenter{
    var contexts:Context?=null
    var views:DiscoverDetailContract.View?=null
    val discoverDetailModel: DiscoverDetailModel by lazy {
        DiscoverDetailModel()
    }
    init {
        this.contexts=context
        this.views=view
    }
    override fun start() {

    }

    override fun requestData(categoryName: String, strategy: String) {
        var observable: Observable<RankBean>?=contexts?.let { discoverDetailModel.loadData(contexts!!,categoryName,strategy) }
        observable?.applySchedulers()?.subscribe{beans:RankBean->
            views?.setData(beans)

        }
    }
    fun requestMoreData(start: Int, categoryName: String, strategy: String){
        var observable:Observable<RankBean>?=contexts?.let { discoverDetailModel.loadMoreData(contexts!!,start,categoryName,strategy) }
        observable?.applySchedulers()?.subscribe{
            beans:RankBean->views?.setData(beans)
        }
    }
}