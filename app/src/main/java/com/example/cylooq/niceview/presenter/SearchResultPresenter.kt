package com.example.cylooq.niceview.presenter

import android.content.Context
import com.example.cylooq.niceview.model.SearchResultModel
import com.example.cylooq.niceview.model.bean.RankBean
import com.example.cylooq.niceview.presenter.contract.SearchResultContract
import com.example.cylooq.niceview.utils.applySchedulers
import io.reactivex.Observable


/**
 * Created by matrix on 17-10-18.
 */
 class SearchResultPresenter(context: Context,view:SearchResultContract.View):SearchResultContract.Presenter{
    var contexts:Context?=null
    var views:SearchResultContract.View?=null
    val model : SearchResultModel by lazy {
        SearchResultModel()
    }
    init {
        this.views=view
        this.contexts=context
    }

    override fun requestData(query: String, star: Int) {
        val observable: Observable<RankBean>?=contexts?.let { model.loadData(contexts!!,query,star) }
        observable?.applySchedulers()?.subscribe{
            bean:RankBean->views?.setData(bean)
        }
    }

    override fun start() {
        //nothing
    }
}