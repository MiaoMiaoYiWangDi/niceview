package com.example.cylooq.niceview.adatper

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

/**
 * Created by RuiChang on 2017/08/29.
 */
class HotAdatper(fragmentManager: FragmentManager, list: ArrayList<Fragment>, title: MutableList<String>) : FragmentPagerAdapter(fragmentManager) {
    var mTitle: MutableList<String> = title
    var mList:ArrayList<Fragment> =list
    override fun getCount(): Int {
        return mList.size
    }
    override fun getItem(position: Int): Fragment {
        return mList[position]
    }

    override fun getPageTitle(position: Int): CharSequence {
       return mTitle[position]
    }
}
