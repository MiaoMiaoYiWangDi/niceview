package com.example.cylooq.niceview.view.fragment.search

import android.app.DialogFragment
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.text.TextUtils
import android.view.*
import android.widget.Toast
import com.example.cylooq.niceview.R

import com.example.cylooq.niceview.adatper.SearchAdatper
import com.example.cylooq.niceview.utils.KeyBoardUtils
import com.example.cylooq.niceview.utils.ToastMine
import com.example.cylooq.niceview.view.SearchResultActivity
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import kotlinx.android.synthetic.main.search_fragment.*
import com.example.cylooq.niceview.utils.showToast

/**
 * Created by matrix on 17-10-17.
 */

const val SEARCH_TAG = "searchFragment"

class SearchFragment : DialogFragment(), SearchFragmentAnim.AnimListener,
        ViewTreeObserver.OnPreDrawListener, DialogInterface.OnKeyListener,
        View.OnClickListener {
    var data : MutableList<String> = arrayListOf("漫威", "清新","匠心","VR","心理学","舞蹈","品牌广告","电影相关",
            "第一视角","教程","毕业设计","奥斯卡","燃","冰与火之歌","温情","线下campaign","公益")
    lateinit var rootView:View
    lateinit var searchFragmentAnim:SearchFragmentAnim
    lateinit var searchAdatper:SearchAdatper
    var toast:Toast?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.DialogStyle)
    }

    override fun onStart() {
        super.onStart()
        initDialog()

    }
    private fun initDialog(){
        val window=dialog.window
        val metrics=resources.displayMetrics
        val width=(metrics.widthPixels*0.98).toInt()//宽
        window!!.setLayout(width,WindowManager.LayoutParams.MATCH_PARENT)
        window.setGravity(Gravity.TOP)
        window.setWindowAnimations(R.style.DialogEmptyAnimation)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        rootView=inflater?.inflate(R.layout.search_fragment,container,false)!!

        return rootView
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setData()
    }
    private fun init(){
        searchFragmentAnim=SearchFragmentAnim()
        searchFragmentAnim.setListener(this)

        dialog.setOnKeyListener(this)
        search_searching.viewTreeObserver.addOnPreDrawListener(this)
        search_searching.setOnClickListener(this)
        search_back.setOnClickListener(this)
    }
    private fun setData(){
        searchAdatper= SearchAdatper(activity,data as ArrayList<String>)
        searchAdatper.setOnDialogDismissListener(object :SearchAdatper.onDialogDismiss{
            override fun onDismiss() {
                hideAnim()
            }
        })
        val manager=FlexboxLayoutManager()
        //设置主轴排列方式
        manager.flexDirection=FlexDirection.ROW
        //是否换行
        manager.flexWrap=FlexWrap.WRAP
        search_recyclerView.layoutManager=manager
        search_recyclerView.itemAnimator=DefaultItemAnimator()
        search_recyclerView.adapter=searchAdatper
    }

    override fun onHideAnimEnd() {
        search_etext_word.setText("")
        dismiss()
    }

    override fun onShowAnimEnd() {
        if(isVisible){
            //打开键盘
            KeyBoardUtils.openKeyboard(activity,search_etext_word)
        }
    }

    private fun hideAnim(){
        KeyBoardUtils.closeKeyboard(activity,search_etext_word)
        searchFragmentAnim.hide(search_searching,rootView)

    }
    private fun search(){
        var inputWord=search_etext_word.text.toString()
        if(TextUtils.isEmpty(inputWord.trim({it <= ' '}))){
            ToastMine(context,"请输入关键字搜索",Toast.LENGTH_SHORT)
        }else{
            hideAnim()
            var searchWord=inputWord.trim()
            var intent= Intent(activity,SearchResultActivity::class.java)
            intent.putExtra("keyWord",searchWord)
            activity?.startActivity(intent)
        }

    }

    override fun onKey(dialog: DialogInterface?, keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode==KeyEvent.KEYCODE_BACK && event?.action == KeyEvent.ACTION_UP){
            hideAnim()
        }
        if(keyCode==KeyEvent.KEYCODE_ENTER && event?.action ==KeyEvent.ACTION_DOWN){
            search()
        }
        return false
    }

    override fun onPreDraw(): Boolean {
        search_searching.viewTreeObserver.removeOnPreDrawListener(this)
        searchFragmentAnim.show(search_searching,rootView)
        return true
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.search_back->{
                hideAnim()
            }
            R.id.search_searching -> {
                search()
            }
        }
    }

}