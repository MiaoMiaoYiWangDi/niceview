package com.example.cylooq.niceview.view.fragment


import android.content.Intent
import android.view.View
import com.example.cylooq.niceview.R
import com.example.cylooq.niceview.view.DownloadActivity
import com.example.cylooq.niceview.view.HistoryActivity
import kotlinx.android.synthetic.main.me_fragment.*

/**
 * Created by RuiChang on 2017/08/28.
 */
class MeFragment:BaseFragment() , View.OnClickListener {
    override fun getLayoutResources(): Int {
        return R.layout.me_fragment
    }
    override fun initView() {
        button_history_me.setOnClickListener(this)
        button_download_me.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.button_history_me ->{
                val intent= Intent(activity,HistoryActivity::class.java)
                startActivity(intent)
            }
            R.id.button_download_me ->{
                val intent=Intent(activity,DownloadActivity::class.java)
                startActivity(intent)
            }
        }
    }
}