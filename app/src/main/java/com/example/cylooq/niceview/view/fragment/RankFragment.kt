package com.example.cylooq.niceview.view.fragment

import android.support.v7.widget.LinearLayoutManager
import com.example.cylooq.niceview.R
import com.example.cylooq.niceview.adatper.RankAdatper
import com.example.cylooq.niceview.model.bean.RankBean
import com.example.cylooq.niceview.presenter.HotPresenter
import com.example.cylooq.niceview.presenter.contract.HotContract
import kotlinx.android.synthetic.main.home_fragment.*
import kotlinx.android.synthetic.main.rank_fragment.*

/**
 * Created by RuiChang on 2017/08/30.
 */
class RankFragment : BaseFragment(), HotContract.View {
    lateinit var mPresenter: HotPresenter
    lateinit var mStrategy: String
    lateinit var mAdapter: RankAdatper
    var mList: ArrayList<RankBean.ItemListBean.DataBean> = ArrayList()
    override fun getLayoutResources(): Int {
        return R.layout.rank_fragment
    }

    override fun initView() {
        recyclerView_rank.layoutManager = LinearLayoutManager(context!!)
        mAdapter = RankAdatper(context, mList)
        recyclerView_rank.adapter = mAdapter
        if (arguments != null) {
            mStrategy = arguments.getString("strategy")
            mPresenter = HotPresenter(context, this)
            mPresenter.requestData(mStrategy)
        }
    }

    override fun setData(bean: RankBean) {
        if (mList.size > 0) {
            mList.clear()
        }
        bean.itemList?.forEach {
            it.data?.let { it1 -> mList.add(it1) }
        }
        mAdapter.notifyDataSetChanged()
    }
}

