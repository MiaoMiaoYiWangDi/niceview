package com.example.cylooq.niceview.adatper

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.cylooq.niceview.R
import com.example.cylooq.niceview.view.SearchResultActivity
import com.google.android.flexbox.FlexboxLayoutManager

/**
 * Created by matrix on 17-10-17.
 */

class SearchAdatper(context: Context, list: ArrayList<String>)
    : RecyclerView.Adapter<SearchAdatper.SearchResultViewHolder>() {
    var contexts: Context? = null
    var lists: ArrayList<String>? = null
    var inflater: LayoutInflater? = null
    var mDialogListener:onDialogDismiss?=null
    init {
        this.contexts = context
        this.lists = list
        this.inflater = LayoutInflater.from(contexts)
    }

    override fun getItemCount(): Int {
        return lists?.size ?: 0
    }

    override fun onBindViewHolder(holder: SearchResultViewHolder?, position: Int) {
        holder?.video_title?.text = lists!![position]
        val params = holder?.video_title?.layoutParams
        if (params is FlexboxLayoutManager.LayoutParams) {
            (holder?.video_title?.layoutParams as FlexboxLayoutManager.LayoutParams).flexGrow=1.0f
        }
        holder?.video_title?.setOnClickListener {
            var searchWord=lists?.get(position)
            var intent= Intent(contexts, SearchResultActivity::class.java)
            intent.putExtra("keyWord",searchWord)
            contexts?.startActivity(intent)
            mDialogListener?.onDismiss()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): SearchResultViewHolder {
        return SearchResultViewHolder(inflater?.inflate(R.layout.item_hot_word, parent, false), contexts!!)
    }

    class SearchResultViewHolder(itemView: View?, context: Context) : RecyclerView.ViewHolder(itemView) {
        var video_title: TextView? = null

        init {
            video_title = itemView?.findViewById<TextView>(R.id.hot_word)
        }
    }
    interface onDialogDismiss{
        fun onDismiss()
    }

    fun setOnDialogDismissListener(onDialogDismiss:onDialogDismiss){
        mDialogListener = onDialogDismiss
    }
}