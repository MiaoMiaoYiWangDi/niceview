package com.example.cylooq.niceview.model.bean

/**
 * Created by RuiChang on 2017/08/28.
 */
data class DiscoverBean(var id: Int, var name: String?, var alias: Any?,
                        var descriptiion: String?, var bgPicture: String?,
                        var bgColor: String?, var headerImage: String?) {
    /**
     * id :36
     * name:发现栏目的名字
     * alias：null
     * description 介绍
     * bgPicture 背景图片
     * bgColor 背景颜色
     * headerImage
     */
}