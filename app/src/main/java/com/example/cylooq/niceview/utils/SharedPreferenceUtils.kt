package com.example.cylooq.niceview.utils

import android.content.Context
import android.content.SharedPreferences
import java.util.*
import kotlin.collections.HashMap

/**
 * Created by RuiChang on 2017/08/25.
 */
class SharedPreferenceUtils private constructor(context: Context, sharedPreferenceName: String) {
    private var sharedPreference: SharedPreferences? = null

    init {
        sharedPreference = context.getSharedPreferences(sharedPreferenceName, Context.MODE_PRIVATE)
    }
    /**sharedPreference写入String
    *@param key 键
    * @param value 值
     */
    fun put(key:String,value:String){
        sharedPreference?.edit()?.putString(key,value)?.apply()
    }

    /**获取String
     * @param key 键
     * @param defaultValue 默认值""
     * @return 返回String，没有""
     */
    @JvmOverloads fun getString(key: String,defaultValue: String=""):String{
        return sharedPreference?.getString(key,defaultValue)?.toString()?:defaultValue
    }

    /**写入Int值
     * @param key 键
     * @param value 值
     */
    fun put(key: String,value:Int){
        sharedPreference?.edit()?.putInt(key,value)?.apply()
    }
    /**获取Int
     * @param key 键
     * @param defaultValue 默认值-1
     * @return 返回Int，没有-1
     */
    @JvmOverloads fun getInt(key: String,defaultValue: Int=-1):Int{
        return sharedPreference?.getInt(key,defaultValue)?:defaultValue
    }
    /**写入Long
     * @param key 键
     * @param value 值
     */
    fun put(key: String,value:Long){
        sharedPreference?.edit()?.putLong(key,value)?.apply()
    }
    /**获取Long
     * @param key 键
     * @param defaultValue 默认值-1L
     * @return 返回Long，没有返回-1L
     */
    @JvmOverloads fun getLong(key: String,defaultValue: Long=-1L):Long{
        return sharedPreference?.getLong(key,defaultValue)?:defaultValue
    }
    /**写入Float
     * @param key 键
     * @param value 值
     */
    fun put(key: String,value:Float){
        sharedPreference?.edit()?.putFloat(key,value)?.apply()
    }
    /**获取Float
     * @param key 键
     * @param defaultValue 默认值-1L
     * @return 返回float，没有返回-1F
     */
    @JvmOverloads fun getFloat(key: String,defaultValue:Float=-1f):Float{
        return sharedPreference?.getFloat(key,defaultValue)?:defaultValue
    }
    /**写入Boolean
     * @param key 键
     * @param value 值
     */
    fun put(key: String,value:Boolean){
        sharedPreference?.edit()?.putBoolean(key,value)?.apply()
    }
    /**获取Booleand
     * @param key 键
     * @param defaultValue 默认值-1L
     * @return 返回布尔值，没有就返回flase
     */
    @JvmOverloads fun getBoolean(key: String,defaultValue: Boolean=false):Boolean{
        return sharedPreference?.getBoolean(key,defaultValue)?:defaultValue
    }
    /**写入String集合
     * @param key 键
     * @param value 值
     */
    fun put(key: String,values:Set<String>){
        sharedPreference?.edit()?.putStringSet(key,values)?.apply()
    }
    /**获取Booleand
     * @param key 键
     * @param defaultValue 默认值-1L
     * @return 返回该键对应的值，没有就返回默认值空String集合
     */
    @JvmOverloads fun getStringSet(key: String,defaultValues:Set<String> = Collections.emptySet()):Set<String>{
        return sharedPreference?.getStringSet(key,defaultValues)?:defaultValues
    }

    /**
     * 获取sharedPreference的所有键值对
     * @return Map<String,*>
     */
    val all:Map<String,*>
        get()=sharedPreference?.all!!

    /**
     * 移除键值对
     * @param key 键
     */
    fun remov(key: String){
        sharedPreference?.edit()?.remove(key)?.apply()
    }

    /**
     * 检测是否存在该键
     * @param key 键
     * @return Boolean 没有返回false
     */
    protected  fun contains(key: String):Boolean{
        return sharedPreference?.contains(key)?:false
    }
    /**
     * 清除所有数据
     */
    fun clear(){
        sharedPreference?.edit()?.clear()?.apply()
    }
    companion object {
        private val sharedPreferenceMap=HashMap<String,SharedPreferenceUtils>()
        /**
         * 获取sharedPreference实例

         * @param spName sp名
         * *
         * @return [SPUtils]
         */
        fun getInstance(context: Context,sharedPreferenceName: String):SharedPreferenceUtils{
            var sharedPrefercenceName=sharedPreferenceName
            if(isSpace(sharedPrefercenceName)) sharedPrefercenceName="sharedPreferenceUtils"
            var sharedPreference:SharedPreferenceUtils?= sharedPreferenceMap[sharedPrefercenceName]
            if(sharedPreference==null){
                sharedPreference= SharedPreferenceUtils(context,sharedPreferenceName)
                sharedPreferenceMap.put(sharedPrefercenceName,sharedPreference)
            }
            return sharedPreference
        }
        //判断字符是否为空和是否为空格
        private fun isSpace(s: String?): Boolean {
            if (s == null) return true
            var i = 0
            val len = s.length
            while (i < len) {
                if (!Character.isWhitespace(s[i])) {
                    return false
                }
                ++i
            }
            return true
        }
    }

}
