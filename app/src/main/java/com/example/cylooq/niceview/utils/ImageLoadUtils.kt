package com.example.cylooq.niceview.utils

import android.content.Context
import android.graphics.Bitmap
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.target.SimpleTarget
import com.example.cylooq.niceview.R

/**
 * Created by RuiChang on 2017/08/25.
 * 加载图片
 * Glide 一个专注于平滑滚动的图片加载和缓存库
 */
class ImageLoadUtils {
    companion object {
        fun displyImage(context: Context,imageView: ImageView?,url:String){
            //判断imageView变量是否为空
            if(imageView==null){
                //一般在使用一些开源或商业的jar包的时候，需要我们以Argument的形式去配置其参数
                //IllegalArgumentException此异常表明向方法传递了一个不合法或不正确的参数。
                throw IllegalArgumentException("argument error:don`t have imageView")
            }
            /*
            *diskCacheStrategy(DiskCacheStrategy strategy).设置缓存策略。DiskCacheStrategy.SOURCE：缓存原始数据，
            DiskCacheStrategy.RESULT：缓存变换(如缩放、裁剪等)后的资源数据，DiskCacheStrategy.NONE：什么都不缓存，
            DiskCacheStrategy.ALL：缓存SOURC和RESULT。默认采用DiskCacheStrategy.RESULT策略，
            对于download only操作要使用DiskCacheStrategy.SOURCE。
            *centerCrop()从图片中心开始，均衡的缩放图像（保持图像原始比例），使得图片充满整个imageview同时保持宽高比例不变
            * crossFade()淡入淡出
             */
            Glide.with(context).load(url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .placeholder(R.drawable.gif_loading)
                    .error(R.drawable.img_empty)
                    .crossFade().into(imageView)
        }
        fun displayHigh(context: Context, imageView: ImageView?, url: String){
            if (imageView == null) {
                throw IllegalArgumentException("argument error")
            }
            Glide.with(context).load(url)
                    .asBitmap()
                    .format(DecodeFormat.PREFER_ARGB_8888)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .placeholder(R.drawable.gif_loading)
                    .error(R.drawable.img_empty)
                    .into(imageView)
        }
/*        fun displayGray(context: Context, view: View?, url: String){
            if (view == null) {
                throw IllegalArgumentException("argument error")
            }
            Glide.with(context).load(url)
                    .asBitmap()
                    .format(DecodeFormat.PREFER_ARGB_8888)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .placeholder(R.drawable.gif_loading)
                    .error(R.drawable.img_empty)
                    .into(SimpleTarget<Bitmap>(180,180){

                    })
        }*/
    }

}