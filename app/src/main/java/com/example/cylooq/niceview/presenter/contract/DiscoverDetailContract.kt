package com.example.cylooq.niceview.presenter.contract

import com.example.cylooq.niceview.model.bean.RankBean
import com.example.cylooq.niceview.presenter.base.BasePresenter
import com.example.cylooq.niceview.presenter.base.BaseView


/**
 * Created by RuiChang on 2017/08/28.
 */
interface DiscoverDetailContract{
    interface Presenter:BasePresenter{
        fun requestData(categoryName: String, strategy: String)
    }
    interface View :BaseView<Presenter>{
        fun setData(bean:RankBean)
    }
}