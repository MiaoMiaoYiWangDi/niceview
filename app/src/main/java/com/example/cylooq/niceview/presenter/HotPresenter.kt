package com.example.cylooq.niceview.presenter

import android.content.Context
import com.example.cylooq.niceview.model.HotModel
import com.example.cylooq.niceview.model.bean.RankBean
import com.example.cylooq.niceview.presenter.contract.HotContract
import com.example.cylooq.niceview.utils.applySchedulers
import io.reactivex.Observable

/**
 * Created by RuiChang on 2017/08/29.
 * 热门
 */
class HotPresenter(context: Context, view: HotContract.View) : HotContract.Presenter {
    var contexts: Context? = null
    var views: HotContract.View? = null
    val hotModel: HotModel by lazy {
        HotModel()
    }

    init {
        contexts = context
        views = view
    }

    override fun requestData(strategy: String) {
        val observable: Observable<RankBean>? = contexts?.let { hotModel?.loadData(contexts!!, strategy) }
        observable?.applySchedulers()?.subscribe() { bean: RankBean ->
            views?.setData(bean)
        }
    }

    override fun start() {

    }
}