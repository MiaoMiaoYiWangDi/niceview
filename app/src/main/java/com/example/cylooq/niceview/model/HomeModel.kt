package com.example.cylooq.niceview.model.bean

import android.content.Context
import com.example.cylooq.niceview.network.ApiService
import com.example.cylooq.niceview.network.RetrofitClient
import io.reactivex.Observable

/**
 * Created by RuiChang on 2017/08/24.
 */
class HomeModel {
    fun loadData(context: Context, isFirst: Boolean, data: String?): Observable<HomeBean>? {
        val retrofitClient = RetrofitClient.getInstance(context, ApiService.BASE_URL)
        val apiService = retrofitClient.create(ApiService::class.java)
        when (isFirst) {
            true -> return apiService?.getHomeData()

            false -> return apiService?.getHomeMoreData(data.toString(), "2")
        }
    }
}