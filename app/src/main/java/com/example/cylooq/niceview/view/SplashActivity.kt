package com.example.cylooq.niceview.view

/**
 * Created by RuiChang on 2017/08/22.
 * 启动界面
 */
import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.view.animation.ScaleAnimation
import com.example.cylooq.niceview.R
import kotlinx.android.synthetic.main.activity_splash.*
import com.example.cylooq.niceview.utils.newIntent
import com.gyf.barlibrary.ImmersionBar

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_splash)

        ImmersionBar.with(this).transparentBar().barAlpha(0f).fitsSystemWindows(true).init()
        val window = window
        val params = window.attributes
        params.systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        window.attributes = params

        setFont()
        setAnimation()
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Log.d("什么呀", "没有权限")
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_SETTINGS),
                    0)
        }
    }

    //设置字体
    private fun setFont() {
        //
        var font: Typeface = Typeface.createFromAsset(this.assets, "fonts/Lobster-1.4.otf")
        name_chinese_splash.typeface = font
        name_english_splash.typeface = font
    }

    //设置动画
    private fun setAnimation() {
        //透明效果
        var alphaAnimation = AlphaAnimation(0.1f, 1.0f)
        alphaAnimation.duration = 1100
        //缩放效果
        var scaleAnimation = ScaleAnimation(0.1f, 1.0f, 0.1f, 1.0f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f)
        scaleAnimation.duration = 1100
        //动画设置，添加动画效果，动画时间(duration)
        var animationSet = AnimationSet(true)
        animationSet.addAnimation(alphaAnimation)
        animationSet.addAnimation(scaleAnimation)
        animationSet.duration = 1100
        //图标关联动画
        icon_activity_splash.startAnimation(animationSet)
        animationSet.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(p0: Animation?) {
            }

            override fun onAnimationStart(p0: Animation?) {
                setTextAniamtion()
            }

            override fun onAnimationEnd(p0: Animation?) {
                newIntent<MainActivity>()
                finish()
            }
        })
    }

    //设置文字的动画
    private fun setTextAniamtion() {
        var textAlphaAnimation = AlphaAnimation(0.0f, 1.0f)
        textAlphaAnimation.duration = 1100
        var textAnimationSet = AnimationSet(true)
        textAnimationSet.addAnimation(textAlphaAnimation)
        textAnimationSet.duration = 1100
        name_english_splash.startAnimation(textAnimationSet)
        name_chinese_splash.startAnimation(textAnimationSet)
        textAnimationSet.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationEnd(p0: Animation?) {
            }

            override fun onAnimationStart(p0: Animation?) {

            }

            override fun onAnimationRepeat(p0: Animation?) {
                finish()
            }
        })
    }
}