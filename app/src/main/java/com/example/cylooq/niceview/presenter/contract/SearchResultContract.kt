package com.example.cylooq.niceview.presenter.contract

import com.example.cylooq.niceview.model.bean.RankBean
import com.example.cylooq.niceview.presenter.base.BasePresenter
import com.example.cylooq.niceview.presenter.base.BaseView

/**
 * Created by matrix on 17-10-18.
 */
interface SearchResultContract{
    interface Presenter:BasePresenter{
        fun requestData(query:String,star:Int)
    }
    interface View:BaseView<Presenter>{
        fun setData(bean:RankBean)
    }
}