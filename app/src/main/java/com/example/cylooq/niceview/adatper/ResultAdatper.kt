package com.example.cylooq.niceview.adatper

import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.cylooq.niceview.R
import com.example.cylooq.niceview.model.bean.RankBean
import com.example.cylooq.niceview.model.bean.VideoBean
import com.example.cylooq.niceview.utils.ImageLoadUtils
import com.example.cylooq.niceview.utils.ObjectSaveUtils
import com.example.cylooq.niceview.utils.SharedPreferenceUtils
import com.example.cylooq.niceview.view.VideoDetailActivity
import java.text.SimpleDateFormat

/**
 * Created by matrix on 17-10-17.
 */
class ResultAdatper(context:Context,list:ArrayList<RankBean.ItemListBean.DataBean>)
    :RecyclerView.Adapter<ResultAdatper.ResultAdatperViewHolder>(){
    var contexts:Context?=null
    var lists:ArrayList<RankBean.ItemListBean.DataBean>?=null
    var inflater:LayoutInflater?=null

    init {
        this.contexts=context
        this.lists=list
        inflater= LayoutInflater.from(contexts)
    }
    override fun getItemCount(): Int {
        return lists?.size ?:0
    }

    override fun onBindViewHolder(holder: ResultAdatperViewHolder?, position: Int) {
        var photoUrl:String?=lists?.get(position)?.cover?.feed
        photoUrl?.let { ImageLoadUtils.displyImage(contexts!!,holder?.video_image,it) }

        var videoTitle=lists?.get(position)?.title
        holder?.video_title?.text=videoTitle

        var videoCategory = lists?.get(position)?.category
        var minute = lists?.get(position)?.duration?.div(60)
        var second = lists?.get(position)?.duration?.minus(minute?.times(60) as Long)
        var releaseTime=lists?.get(position)?.releaseTime
        var formatDate= SimpleDateFormat("MM-dd")
        var videoReleaseDate=formatDate.format(releaseTime)
        var realMinute: String
        var realSecond: String
        if (minute!! < 10) {
            realMinute = "0" + minute.toString()
        } else {
            realMinute = minute.toString()
        }
        if (second!! < 10) {
            realSecond = "0" + second.toString()
        } else {
            realSecond = second.toString()
        }
        holder?.video_time?.text=" $videoCategory /$realMinute:$realSecond / $videoReleaseDate"
        holder?.itemView?.setOnClickListener {

            //视频跳转详情
            var intent: Intent = Intent(contexts, VideoDetailActivity::class.java)
            var id:Int=lists?.get(position)?.id?:0
            var videoDesc=lists?.get(position)?.description
            var videoDuration=lists?.get(position)?.duration
            var videoPlayUrl=lists?.get(position)?.playUrl
            var videoBlurred=lists?.get(position)?.cover?.blurred
            var videoFavoriteNum=lists?.get(position)?.consumption?.collectionCount
            var videoShareNum=lists?.get(position)?.consumption?.shareCount
            var videoReplayNum=lists?.get(position)?.consumption?.replyCount
            var videoTime=System.currentTimeMillis()

            var videoBean= VideoBean(id,photoUrl,videoTitle,videoDesc,videoDuration,videoPlayUrl,
                    videoCategory,videoBlurred, videoFavoriteNum,videoShareNum,videoReplayNum,videoTime)
            var cacheUrl= SharedPreferenceUtils.getInstance(contexts!!,"beans").getString(videoPlayUrl!!)
            if (cacheUrl.equals("")){
                var count= SharedPreferenceUtils.getInstance(contexts!!,"bean").getInt("count")
                if(count==-1){
                    count=count.inc()
                }else{
                    count=1
                }
                SharedPreferenceUtils.getInstance(contexts!!,"beans").put("count",count)
                SharedPreferenceUtils.getInstance(contexts!!,"beans").put(videoPlayUrl!!,videoPlayUrl)
                ObjectSaveUtils.saveObject(contexts!!,"bean$count",videoBean)

            }
            intent.putExtra("data",videoBean as Parcelable)
            contexts?.let { contexts ->contexts.startActivity(intent)  }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ResultAdatperViewHolder {
        return ResultAdatperViewHolder(inflater?.inflate(R.layout.item_search_result,parent,false),contexts!!)
    }

    class ResultAdatperViewHolder(itemView : View?, contex: Context):RecyclerView.ViewHolder(itemView){
        var video_title:TextView?=null
        var video_image:ImageView?=null
        var video_time:TextView?=null
        init {
            video_image=itemView?.findViewById(R.id.result_video_image)
            video_title=itemView?.findViewById(R.id.result_voide_title)
            video_time=itemView?.findViewById(R.id.result_video_time)
        }
    }
}