package com.example.cylooq.niceview.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.example.cylooq.niceview.R
import com.example.cylooq.niceview.adatper.RankAdatper
import com.example.cylooq.niceview.model.bean.RankBean
import com.example.cylooq.niceview.presenter.DiscoverDetailPresenter
import com.example.cylooq.niceview.presenter.contract.DiscoverDetailContract
import com.gyf.barlibrary.ImmersionBar
import kotlinx.android.synthetic.main.activity_discover_datail.*
import java.util.regex.Pattern

/**
 * Created by RuiChang on 2017/08/28.
 */
class DiscoverDetailActivity : AppCompatActivity(), DiscoverDetailContract.View, SwipeRefreshLayout.OnRefreshListener {
    lateinit var discoverDetailPresenter: DiscoverDetailPresenter
    lateinit var discoverDetailAdatper: RankAdatper
    lateinit var data: String
    var isRefresh: Boolean = false

    var star:Int =10
    var list: ArrayList<RankBean.ItemListBean.DataBean> = ArrayList()
    lateinit var name: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_discover_datail)
        ImmersionBar.with(this).transparentBar().barAlpha(0f).statusBarDarkFont(true).fitsSystemWindows(true).init()
       // ImmersionBar.with(this).fitsSystemWindows(true).init()
        setToolBar()

        recylerview_discover_detail.layoutManager = LinearLayoutManager(this)
        discoverDetailAdatper = RankAdatper(this, list)
        refreshLayout_discover_datail.setOnRefreshListener(this)
        recylerview_discover_detail.adapter = discoverDetailAdatper
        discoverDetailPresenter = DiscoverDetailPresenter(this, this)

        discoverDetailPresenter?.requestData(name, "date")
        recylerview_discover_detail.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                var layoutMannager: LinearLayoutManager = recyclerView?.layoutManager as LinearLayoutManager
                var lastPositon = layoutMannager.findLastVisibleItemPosition()
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastPositon == list?.size - 1) {
                    if (data !== null) {
                        discoverDetailPresenter.requestMoreData(star, name, "date")
                        star = star.plus(10)
                    }
                }
            }
        })
    }


    override fun setData(bean: RankBean) {
        val regEx = "[^0-9]"
        val pattern = Pattern.compile(regEx)
        val patternMatcher = pattern.matcher(bean?.nextPageUrl as CharSequence?)
        data = patternMatcher.replaceAll("")?.subSequence(1, patternMatcher.replaceAll("").length - 1).toString()
        //截取json的数据
        if(isRefresh){
            isRefresh=false
            refreshLayout_discover_datail.isRefreshing=false
            if(list?.size>0){
                list?.clear()
            }
        }
        bean?.itemList?.forEach{
            it?.data?.let { it1->list.add(it1) }
        }
        discoverDetailAdatper?.notifyDataSetChanged()
    }

    override fun onRefresh() {
        if(!isRefresh){
            isRefresh=true
            discoverDetailPresenter.requestData(name,"date")
        }
    }
    private fun setToolBar(){
        setSupportActionBar(toolbar_discover_detail)
        var toolBar=supportActionBar
        intent?.getStringExtra("name")?.let {
            name=intent.getStringExtra("name")
            toolBar?.title=name
        }
        toolBar?.setDisplayHomeAsUpEnabled(true)
        toolbar_discover_detail.setNavigationOnClickListener{
            onBackPressed()
        }
    }

}