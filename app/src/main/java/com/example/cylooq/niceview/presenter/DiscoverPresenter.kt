package com.example.cylooq.niceview.presenter

import android.content.Context
import com.example.cylooq.niceview.model.DiscoverModel
import com.example.cylooq.niceview.model.bean.DiscoverBean
import com.example.cylooq.niceview.presenter.contract.DiscoverContract
import com.example.cylooq.niceview.utils.applySchedulers
import io.reactivex.Observable

/**
 * Created by RuiChang on 2017/08/28.
 */
class DiscoverPresenter(contexts: Context,views:DiscoverContract.View):DiscoverContract.Presenter{
    var context:Context?=null
    var view:DiscoverContract.View?=null
    val discoverModel : DiscoverModel by lazy {
        DiscoverModel()
    }
    init {
        this.view=views
        this.context=contexts
    }

    override fun start() {
        requestData()
    }

    override fun requestData() {
        val observable:Observable<MutableList<DiscoverBean>>?=context?.let { discoverModel.loadData(context!!) }
        observable?.applySchedulers()?.subscribe{beans :MutableList<DiscoverBean> ->
            view?.setData(beans)

        }
    }

}