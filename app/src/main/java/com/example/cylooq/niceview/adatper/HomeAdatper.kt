package com.example.cylooq.niceview.adatper

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Parcelable
import android.support.v7.widget.RecyclerView
import com.example.cylooq.niceview.model.bean.HomeBean
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.cylooq.niceview.R
import com.example.cylooq.niceview.model.bean.VideoBean
import com.example.cylooq.niceview.utils.ImageLoadUtils
import com.example.cylooq.niceview.utils.ObjectSaveUtils
import com.example.cylooq.niceview.utils.SharedPreferenceUtils
import com.example.cylooq.niceview.view.VideoDetailActivity

/**
 * Created by RuiChang on 2017/08/25.
 * 主页精选视频数据适配器
 */
class HomeAdatper(context: Context, list: MutableList<HomeBean.IssueListBean.ItemListBean>?) : RecyclerView.Adapter<HomeAdatper.HomeViewHolder>() {
    var context: Context? = null
    var list: MutableList<HomeBean.IssueListBean.ItemListBean>? = null
    var inflater: LayoutInflater? = null
    var text:String?=null

    init {
        this.context = context
        this.list = list
        this.inflater = LayoutInflater.from(context)
        this.text=context.getString(R.string.Release)?:""
    }

    //获取item项的个数，没有返回0
    override fun getItemCount(): Int {
        return list?.size ?: 0
    }

    //inflate(R.layout.xxx,patent,false)读取resource文件夹里的xml文件 ，相当于findViewById
    //不同点，inflate是未加载的xml文件，findViewById是已经加载的xml文件里id值
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): HomeViewHolder {
        return HomeViewHolder(inflater?.inflate(R.layout.item_home,parent,false), context!!)
    }

    override fun onBindViewHolder(holder: HomeViewHolder?, position: Int) {
        var listBean = list?.get(position)
        var videoTitle = listBean?.data?.title
        var videoCategory = listBean?.data?.category
        var minute = listBean?.data?.duration?.div(60)
        var second = listBean?.data?.duration?.minus(minute?.times(60) as Long)
        var realMinute: String
        var realSecond: String
        if (minute!! < 10) {
            realMinute = "0" + minute.toString()
        } else {
            realMinute = minute.toString()
        }
        if (second!! < 10) {
            realSecond = "0" + second.toString()
        } else {
            realSecond = second.toString()
        }
        var videoPhoto=listBean?.data?.cover?.feed
        ImageLoadUtils.displyImage(context!!,holder?.img_photo_home,videoPhoto as String)
        holder?.video_title_home?.text=videoTitle
        holder?.video_detail_home?.text="$text $videoCategory /$realMinute:$realSecond"

        holder?.itemView?.setOnClickListener {

            //视频跳转详情
            var intent:Intent=Intent(context,VideoDetailActivity::class.java)
            var id:Int=listBean?.data?.id?:0
            var videoDesc=listBean?.data?.description
            var videoDuration=listBean?.data?.duration
            var videoPlayUrl=listBean?.data?.playUrl
            var videoBlurred=listBean?.data?.cover?.blurred
            var videoFavoriteNum=listBean?.data?.consumption?.collectionCount
            var videoShareNum=listBean?.data?.consumption?.shareCount
            var videoReplayNum=listBean?.data?.consumption?.replyCount
            var videoTime=System.currentTimeMillis()

            var videoBean= VideoBean(id,videoPhoto,videoTitle,videoDesc,videoDuration,videoPlayUrl,
                    videoCategory,videoBlurred, videoFavoriteNum,videoShareNum,videoReplayNum,videoTime)
            var cacheUrl=SharedPreferenceUtils.getInstance(context!!,"beans").getString(videoPlayUrl!!)
            if (cacheUrl.equals("")){
                var count=SharedPreferenceUtils.getInstance(context!!,"bean").getInt("count")
                if(count==-1){
                    count=count.inc()
                }else{
                    count=1
                }
                SharedPreferenceUtils.getInstance(context!!,"beans").put("count",count)
                SharedPreferenceUtils.getInstance(context!!,"beans").put(videoPlayUrl!!,videoPlayUrl)
                ObjectSaveUtils.saveObject(context!!,"bean$count",videoBean)

            }
            intent.putExtra("data",videoBean as Parcelable)
            context?.let { context ->context.startActivity(intent)  }

        }

    }

    class HomeViewHolder(itemView: View?,context: Context) : RecyclerView.ViewHolder(itemView) {
        var video_title_home: TextView? = null
        var video_detail_home: TextView? = null
        var video_time_home: TextView? = null
        var img_photo_home: ImageView? = null

        init {
            /*img_photo_home = itemView?.findViewById(R.id.img_photo_home) as ImageView?
            *在sdk26之后就不行了，新版本的 findViewById ，其返回类型是<t extends View>，这是一个泛型的声明
            * ，具体类型则是根据所赋值的变量类型来确定的。
            * 上述代码则由于没有足够的信息来推导findViewById的返回类型
            */
            video_title_home = itemView?.findViewById<TextView>(R.id.video_title_home)
            video_detail_home = itemView?.findViewById<TextView>(R.id.video_detail_home)
            img_photo_home = itemView?.findViewById<ImageView>(R.id.img_photo_home)

        }
    }
}
