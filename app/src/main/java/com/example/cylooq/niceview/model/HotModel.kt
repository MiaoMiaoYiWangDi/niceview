package com.example.cylooq.niceview.model

import android.content.Context

import com.example.cylooq.niceview.model.bean.RankBean
import com.example.cylooq.niceview.network.ApiService
import com.example.cylooq.niceview.network.RetrofitClient
import io.reactivex.Observable

/**
 * Created by RuiChang on 2017/08/29.
 */
class HotModel{
    fun loadData(context: Context,strategy:String?): Observable<RankBean>?{
        var retrofitClient=RetrofitClient.getInstance(context,ApiService.BASE_URL)
        var apiService=retrofitClient?.create(ApiService::class.java)
        return apiService?.getHotData(10,strategy!!,"26868b32e808498db32fd51fb422d00175e179df",83)
    }
}